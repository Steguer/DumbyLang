use std::mem::size_of;

pub mod lexer;
pub mod parser;

fn main() {
    let test = String::from("1 + 1 = 2");
    let tokens = lexer::read_char(test);
    for t in tokens {
        println!("Token value: {:?}", t);
    }

    println!("Hello, world!");
}


