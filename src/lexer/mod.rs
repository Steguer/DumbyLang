#[cfg(test)]
mod tests;

#[derive(Debug, PartialEq, Clone)]
pub enum Token {
    Illegal,
    Eof,
    Eol,
    Ident,
    Int(u32),
    Assign,
    Plus,
    Comma,
    Semicolon,
    LParen,
    RParen,
    LBrace,
    RBrace,
    Function,
    Let,
    Identifier(String)
}

struct Lexer {
    input: String,
    position: u32,
    read_position: usize,
    ch: u8
}

// Only support ascii for the moment
pub fn read_char(input: String) -> Vec<Token> {
    let mut result = Vec::new();
    let mut buffer = String::new();
    let mut i = 0;
    let chars = input.chars();
    let number = input.len();
    let mut eol = false;

    println!("Input string is: {}", input);
    println!("Number of characters: {}", number);

    for c in chars {
        i += 1;

        // TODO: string type
        if c == '\n' {
            println!("The char is a line feed");
            eol = true;
        }
        else if !c.is_whitespace() && c != '\r' {
            println!("The char is not a whitespace or a carriage return");
            buffer.push(c);
            if i < number {
                continue;
            }
        }

        println!("Current buffer: {}", buffer);

        match tokenize(buffer.as_str()) {
            Some(Token::Illegal) => {
                panic!("Illegal token character {}", c)
            },
            Some(token) => {
                result.push(token);
                buffer.clear();
            },
            None => ()
        }

        if eol {
            result.push(Token::Eol);
            eol = false
        }
    }

    return result;
}

fn tokenize(buffer: &str) -> Option<Token> {
    if buffer.is_empty() {
        println!("Buffer was empty");
        return None
    }

    match buffer {
        "+" => Some(Token::Plus),
        "," => Some(Token::Comma),
        "{" => Some(Token::LBrace),
        "}" => Some(Token::RBrace),
        ";" => Some(Token::Semicolon),
        "=" => Some(Token::Assign),
        "(" => Some(Token::LParen),
        ")" => Some(Token::RParen),
        "let" => Some(Token::Let),
        _ => {
            let t = buffer.parse::<u32>();
            match t {
                Err(e) => {
                    Some(Token::Identifier(buffer.to_string()))
                },
                Ok(i) => {
                    Some(Token::Int(i))
                }
            }
        }
    }
}
