use super::*;

#[test]
fn test_simple_mathematical_expression() {
    let test = String::from("1 + 1 = 2");
    let tokens = read_char(test);

    assert_eq!(5, tokens.len());
    assert_eq!(Token::Int(1), tokens[0]);
    assert_eq!(Token::Plus, tokens[1]);
    assert_eq!(Token::Int(1), tokens[2]);
    assert_eq!(Token::Assign, tokens[3]);
    assert_eq!(Token::Int(2), tokens[4]);
}

#[test]
fn test_variable_assign() {
    let test = String::from("test = 2");
    let tokens = read_char(test);

    assert_eq!(3, tokens.len());
    assert_eq!(Token::Identifier(String::from("test")), tokens[0]);
    assert_eq!(Token::Assign, tokens[1]);
    assert_eq!(Token::Int(2), tokens[2]);
}

#[test]
fn test_let_assign() {
    let test = String::from("let test = 2");
    let tokens = read_char(test);

    assert_eq!(tokens.len(), 4);
    assert_eq!(tokens[0], Token::Let);
    assert_eq!(tokens[1], Token::Identifier(String::from("test")));
    assert_eq!(tokens[2], Token::Assign);
    assert_eq!(tokens[3], Token::Int(2));
}

#[test]
fn test_let_assign_with_lf() {
    let test = String::from("let test = 2\n");
    let tokens = read_char(test);

    assert_eq!(tokens.len(), 5);
    assert_eq!(tokens[0], Token::Let);
    assert_eq!(tokens[1], Token::Identifier(String::from("test")));
    assert_eq!(tokens[2], Token::Assign);
    assert_eq!(tokens[3], Token::Int(2));
}

#[test]
fn test_let_assign_with_eol() {
    let test = String::from("let test = 2\r\n");
    let tokens = read_char(test);

    assert_eq!(tokens.len(), 5);
    assert_eq!(tokens[0], Token::Let);
    assert_eq!(tokens[1], Token::Identifier(String::from("test")));
    assert_eq!(tokens[2], Token::Assign);
    assert_eq!(tokens[3], Token::Int(2));
}

#[test]
fn test_multiple_let_assign_with_eol() {
    let test = String::from("let test = 2\r\nlet test = 2");
    let tokens = read_char(test);

    assert_eq!(tokens.len(), 9);
    assert_eq!(tokens[0], Token::Let);
    assert_eq!(tokens[1], Token::Identifier(String::from("test")));
    assert_eq!(tokens[2], Token::Assign);
    assert_eq!(tokens[3], Token::Int(2));
    assert_eq!(tokens[4], Token::Eol);
    assert_eq!(tokens[5], Token::Let);
    assert_eq!(tokens[6], Token::Identifier(String::from("test")));
    assert_eq!(tokens[7], Token::Assign);
    assert_eq!(tokens[8], Token::Int(2));
}
