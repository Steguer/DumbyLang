#[cfg(test)]
mod tests;

use lexer::Token;
use std::io::Error;
use std::{fmt, error};
use parser::Node::LetAssignment;

enum Node {
    Root(Vec<Node>),
    LetAssignment(Identifier, Expression)
}

struct Identifier {
    token: Token
}

struct Expression {
    tokens: Vec<Token>,
}

#[derive(Debug)]
enum ParseErrorKind {
    AssignNotFound
}

#[derive(Debug)]
struct ParseError {
    err: ParseErrorKind
}

impl fmt::Display for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let err_message = match self.err {
            ParseErrorKind::AssignNotFound => "Identifier should be follow by an assignment."
        };
        write!(f, "Parse error: {}", err_message)
    }
}

fn parse(input: Vec<Token>) -> Result<Node, ParseError> {
    let mut root = Vec::with_capacity(input.len());
    let mut iterator = input.iter();

    while let Some(t) = iterator.next() {
         match t {
             Token::Let => {
                 let identifier_token = match iterator.next() {
                     Some(token) => token,
                     None => continue
                 };

                 if let Some(token) = iterator.next() {
                     match token {
                         Token::Assign => (),
                         _ => return Err(ParseError { err: ParseErrorKind::AssignNotFound })
                     }
                 };

                 let mut expression_tokens = Vec::with_capacity(input.len());
                 while let Some(token) = iterator.next() {
                     if let Token::Eol | Token::Eof = token {
                         break
                     }
                     expression_tokens.push(token.clone());
                 }
                 let identifier = Identifier{ token: identifier_token.clone() };
                 let expression = Expression{ tokens: expression_tokens };

                 root.push(Node::LetAssignment(identifier, expression))
             }
             _ => {
                 return Err(ParseError { err: ParseErrorKind::AssignNotFound} )
             }
         }
    }
    return Ok(Node::Root(root));
}