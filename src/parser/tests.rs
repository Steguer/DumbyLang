use super::*;

#[test]
fn test_parse_let_statement(){
    let tokens = vec![
        Token::Let,
        Token::Identifier(String::from("test")),
        Token::Assign,
        Token::Int(1),
        Token::Plus,
        Token::Int(2),
        Token::Eol
    ];

    match parse(tokens) {
        Ok(result) => {
            if let Node::Root(nodes) = result {
                if let Node::LetAssignment(identifier, expression ) = &nodes[0] {
                    assert_eq!(identifier.token, Token::Identifier(String::from("test")));
                    assert_eq!(expression.tokens.len(), 3);
                    assert_eq!(expression.tokens[0], Token::Int(1));
                    assert_eq!(expression.tokens[1], Token::Plus);
                    assert_eq!(expression.tokens[2], Token::Int(2));
                } else {
                    assert!(false, "The node was not a Let assignment.")
                }
            } else {
                assert!(false, "{}", "First node was not a root.")
            }
        },
        Err(error) => {
            assert!(false, "{}", error)
        }
    }
}
